# Appdirect integration challenge

#Features implemented :
###Subscription Notification Endpoint :
            create
            change
            cancel
            status
###User Assignment Endpoint :
          assign 
          unassign
###Single Sign On suport with the AppDirect OpenID provider


###Deploymment
   Application is deploy on heroku on https://murmuring-inlet-17777.herokuapp.com/

## 

###Subscription Create Notification URL
https://murmuring-inlet-17777.herokuapp.com/appdirect/subscription/create?url={eventUrl}

###Subscription Change Notification URL
https://murmuring-inlet-17777.herokuapp.com/appdirect/subscription/change?url={eventUrl}

###Subscription Cancel Notification URL
https://murmuring-inlet-17777.herokuapp.com/appdirect/subscription/cancel?url={eventUrl}

###Subscription Status Notification URL
https://murmuring-inlet-17777.herokuapp.com/appdirect/subscription/status?url={eventUrl}

###User Assignment Notification URL
https://murmuring-inlet-17777.herokuapp.com/appdirect/user/assign?url={eventUrl}

###User Unassignment Notification URL
https://murmuring-inlet-17777.herokuapp.com/appdirect/user/unassign?url={eventUrl}

###Authentication Login URL
https://murmuring-inlet-17777.herokuapp.com/login/openid?openid_identifier={openid}

###Authentication Logout URL
https://murmuring-inlet-17777.herokuapp.com//logout