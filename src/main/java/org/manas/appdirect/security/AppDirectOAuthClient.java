package org.manas.appdirect.security;

import org.manas.appdirect.model.enums.SubscriptionEventType;
import org.manas.appdirect.model.request.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth.consumer.ProtectedResourceDetails;
import org.springframework.security.oauth.consumer.client.OAuthRestTemplate;
import org.springframework.stereotype.Component;
/**
 * 
 * @author Manas
 *
 */
@Component
public class AppDirectOAuthClient {

    @Autowired
    private ProtectedResourceDetails protectedResourceDetails;

    public Event getNotification(String url, SubscriptionEventType type) {
        final OAuthRestTemplate rest = new OAuthRestTemplate(protectedResourceDetails);
        final Event event = rest.getForObject(url, Event.class);
        if (!type.equals(event.getType())) {
            //throw new BadNotificationType(type, SubscriptionEventType.type);
        }
        return event;
    }

}