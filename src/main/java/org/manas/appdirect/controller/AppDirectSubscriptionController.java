package org.manas.appdirect.controller;

import java.util.List;

import org.manas.appdirect.model.dao.SubscriptionDao;
import org.manas.appdirect.model.request.SubscriptionEvent;
import org.manas.appdirect.model.response.EventResult;
import org.manas.appdirect.repository.SubscriptionRespository;
import org.manas.appdirect.service.base.SubscriptionService;
import org.manas.appdirect.service.base.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * @author Manas
 *
 */

@RestController
@RequestMapping("/appdirect/subscription")
public class AppDirectSubscriptionController {

	@Autowired
	private UserService userService;

	@Autowired
	private SubscriptionService subscriptionService;
	
	@Autowired
	private SubscriptionRespository subscriptionRepository;

	private static final Logger logger = LoggerFactory
			.getLogger(AppDirectSubscriptionController.class);

	@RequestMapping(value = "/create", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody EventResult createSubscription(
			@RequestParam(value = "url", required = false) String eventUrl)
			throws Exception {

		logger.info("url received for create subscription is : " + eventUrl);

		EventResult result = subscriptionService.createSubscription(eventUrl,
				new SubscriptionEvent());
		return result;
	}

	@RequestMapping(value = "/change", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody EventResult changeSubscription(
			@RequestParam(value = "url", required = false) String eventUrl)
			throws Exception {

		logger.info("url received for change subscription is : " + eventUrl);

		EventResult EventResult = subscriptionService.changeSubscription(
				eventUrl, new SubscriptionEvent());

		return EventResult;
	}

	@RequestMapping(value = "/cancel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody EventResult cancelSubscription(
			@RequestParam(value = "url", required = false) String eventUrl)
			throws Exception {
		logger.info("url received for cancel subscription is : " + eventUrl);

		EventResult EventResult = subscriptionService.cancelSubscription(
				eventUrl, new SubscriptionEvent());

		return EventResult;
	}

	@RequestMapping(value = "/status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody EventResult subscriptionStatus(
			@RequestParam(value = "url", required = false) String eventUrl)
			throws Exception {
		logger.info("url received for status subscription is : " + eventUrl);

		EventResult EventResult = subscriptionService.subscriptionNotice(
				eventUrl, new SubscriptionEvent());
		return EventResult;
	}
	
	@RequestMapping("/list")
    public List<SubscriptionDao> listSubscriptions() {
        return subscriptionRepository.findAll();
    }

   

}
