package org.manas.appdirect.controller;

import java.util.List;

import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.request.UserEvent;
import org.manas.appdirect.model.response.EventResult;
import org.manas.appdirect.repository.UserRepository;
import org.manas.appdirect.service.base.UserService;
import org.manas.appdirect.service.impl.UserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/appdirect/user")
public class AppDirectUserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserRepository userRepository;

	private static final Logger logger = LoggerFactory
			.getLogger(UserServiceImpl.class);

	@RequestMapping("/assign")
	public EventResult assignUser(@RequestParam("url") String eventUrl) {

		logRequest("assignUser", "eventUrl is : " + eventUrl);
		UserEvent userEvent = userService.retrieveUserEvent(eventUrl);

		return userService.assignUser(userEvent);
	}

	@RequestMapping("/unassign")
	public EventResult unassignUser(@RequestParam("url") String eventUrl) {

		logRequest("assignUser", "eventUrl is : " + eventUrl);

		UserEvent userEvent = userService.retrieveUserEvent(eventUrl);
		return userService.unAssignUser(userEvent);
	}

	@RequestMapping("/list")
	public List<UserDao> listUsers() {
		return userRepository.findAll();
	}

	private void logRequest(String methodName, String message) {
		logger.info("AppDirectUserController : Method Called : "+methodName+" log : "+message);
	}
}
