package org.manas.appdirect.model.request;

import javax.persistence.Embedded;

/**
 * @author Manas
 *
 */

public class User {

	protected String email;
	
	protected String firstName;
	
	protected String language;
	
	protected String lastName;
	
	protected String locale;
	
	protected String openId;
	
	protected String uuid;
	
	protected Address address;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "UserInfo [email=" + email + ", firstName=" + firstName + ", language=" + language + ", lastName="
				+ lastName + ", locale=" + locale + ", openId=" + openId + ", uuid=" + uuid + ", address=" + address
				+ "]";
	}
	
	
}
