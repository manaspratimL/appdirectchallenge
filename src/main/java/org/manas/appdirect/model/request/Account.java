package org.manas.appdirect.model.request;

import org.manas.appdirect.model.enums.AccountStatus;


/**
 * 
 * @author Manas
 *
 */
public class Account {
	
	private String accountIdentifier;
	
	private AccountStatus status;

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return "AccountInfo [accountIdentifier=" + accountIdentifier + ", status=" + status + "]";
	}

	
}
