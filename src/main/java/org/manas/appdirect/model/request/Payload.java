package org.manas.appdirect.model.request;


/**
 * @author Manas
 *
 */

public class Payload {

	private User user;

	private Account account;

	private Company company;

	private Order order;

	private Notice notice;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Notice getNotice() {
		return notice;
	}

	public void setNotice(Notice notice) {
		this.notice = notice;
	}

	@Override
	public String toString() {
		return "Payload [user=" + user + ", account=" + account + ", company="
				+ company + ", order=" + order + ", notice=" + notice + "]";
	}

}
