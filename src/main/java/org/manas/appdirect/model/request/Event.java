package org.manas.appdirect.model.request;

import org.manas.appdirect.model.enums.SubscriptionEventType;
/**
 * 
 * @author Manas
 *
 */
public class Event {
	private SubscriptionEventType type;

	private MarketPlace marketplace;

	private String applicationUuid;

	private String flag;

	private String returnUrl;

	private User creator;

	private Payload payload;

	public SubscriptionEventType getType() {
		return type;
	}

	public void setType(SubscriptionEventType type) {
		this.type = type;
	}

	public MarketPlace getMarketplace() {
		return marketplace;
	}

	public void setMarketplace(MarketPlace marketplace) {
		this.marketplace = marketplace;
	}

	public String getApplicationUuid() {
		return applicationUuid;
	}

	public void setApplicationUuid(String applicationUuid) {
		this.applicationUuid = applicationUuid;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

	public User getCreator() {
		return creator;
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public Payload getPayload() {
		return payload;
	}

	public void setPayload(Payload payload) {
		this.payload = payload;
	}

}
