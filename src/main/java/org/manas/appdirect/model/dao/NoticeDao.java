package org.manas.appdirect.model.dao;

import org.manas.appdirect.model.enums.NoticeType;

/**
 * @author Manas
 *
 */

public class NoticeDao {

	public NoticeType type;
	
	public String message;

	public NoticeType getType() {
		return type;
	}

	public void setType(NoticeType type) {
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Notice [type=" + type + ", message=" + message + "]";
	}
	
	
}
