package org.manas.appdirect.model.dao;

import javax.persistence.Embeddable;

/**
 * 
 * @author Manas
 *
 */

@Embeddable
public class MarketPlaceDao {
	
	private String partner;
	
	private String baseUrl;

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	public String getBaseUrl() {
		return baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	@Override
	public String toString() {
		return "MarketPlace [partner=" + partner + ", baseUrl=" + baseUrl + "]";
	}
}
