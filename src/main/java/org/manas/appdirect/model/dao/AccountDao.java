package org.manas.appdirect.model.dao;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;

import org.manas.appdirect.model.enums.AccountStatus;


/**
 * 
 * @author Manas
 *
 */
@Embeddable
public class AccountDao {
	
	//@EmbeddedId
	private String accountIdentifier;
	
	private AccountStatus status;

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}

	public AccountStatus getStatus() {
		return status;
	}

	public void setStatus(AccountStatus status) {
		this.status = status;
	}

	public static AccountDao createNewAccount(String accountIdentifier) {
		AccountDao account = new AccountDao();
        account.setAccountIdentifier(accountIdentifier);
        account.setStatus(AccountStatus.ACTIVE);
        return account;
    }
	
	@Override
	public String toString() {
		return "AccountInfo [accountIdentifier=" + accountIdentifier + ", status=" + status + "]";
	}

	
}
