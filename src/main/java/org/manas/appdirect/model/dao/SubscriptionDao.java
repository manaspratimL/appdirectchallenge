package org.manas.appdirect.model.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author Manas
 *
 */
@Entity
@Table(name = "subscription")
public class SubscriptionDao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String applicationUuid;

	private String flag;

	private String returnUrl;

	@Embedded
	private AccountDao account;

	//@ManyToOne(cascade = CascadeType.PERSIST)
	@Embedded
	private CompanyDao company;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name="subscription_id")
	@MapKey(name="uuid")
	private Map<String, UserDao> userDaos;

	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="user_id")
	private UserDao creator;

	@Embedded
	private MarketPlaceDao marketplace;

	@Embedded
	private OrderDao order;

	private UUID addOnInstanceId;

	private UUID addOnBindingId;

	public Long getId() {
		return id;
	}

	public AccountDao getAccount() {
		return account;
	}

	public void setAccount(AccountDao account) {
		this.account = account;
	}

	public CompanyDao getCompany() {
		return company;
	}

	public void setCompany(CompanyDao company) {
		this.company = company;
	}

	public Map<String, UserDao> getUsers() {
		if (null == userDaos) {
			return new HashMap<String, UserDao>();
		}
		return userDaos;
	}

	public void setUsers(Map<String, UserDao> userDaos) {
		this.userDaos = userDaos;
	}

	public UserDao getCreator() {
		return creator;
	}

	public void setCreator(UserDao creator) {
		this.creator = creator;
	}

	public OrderDao getOrder() {
		return order;
	}

	public void setOrder(OrderDao order) {
		this.order = order;
	}

	public UUID getAddOnInstanceId() {
		return addOnInstanceId;
	}

	public void setAddOnInstanceId(UUID addOnInstanceId) {
		this.addOnInstanceId = addOnInstanceId;
	}

	public UUID getAddOnBindingId() {
		return addOnBindingId;
	}

	public void setAddOnBindingId(UUID addOnBindingId) {
		this.addOnBindingId = addOnBindingId;
	}

	public MarketPlaceDao getMarketplace() {
		return marketplace;
	}

	public void setMarketplace(MarketPlaceDao marketplace) {
		this.marketplace = marketplace;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getApplicationUuid() {
		return applicationUuid;
	}

	public void setApplicationUuid(String applicationUuid) {
		this.applicationUuid = applicationUuid;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
}
