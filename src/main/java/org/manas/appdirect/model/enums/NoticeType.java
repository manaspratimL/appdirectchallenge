package org.manas.appdirect.model.enums;

/**
 * @author Manas
 *
 */

public enum NoticeType {
	REACTIVATED, DEACTIVATED, CLOSED
}
