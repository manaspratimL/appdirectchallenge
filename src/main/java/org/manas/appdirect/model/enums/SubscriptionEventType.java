package org.manas.appdirect.model.enums;

/**
 * @author Manas
 *
 */

public enum SubscriptionEventType {
	
	SUBSCRIPTION_ORDER, SUBSCRIPTION_CHANGE, SUBSCRIPTION_CANCEL, 
	SUBSCRIPTION_NOTICE, USER_ASSIGNMENT, USER_UNASSIGNMENT, USER_UPDATED

}
