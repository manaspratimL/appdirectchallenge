package org.manas.appdirect.model.enums;

/**
 * 
 * @author Manas
 *
 */
public enum AccountStatus {
	
	INITIALIZED, FAILED, FREE_TRIAL, FREE_TRIAL_EXPIRED, ACTIVE, SUSPENDED, CANCELLED

}
