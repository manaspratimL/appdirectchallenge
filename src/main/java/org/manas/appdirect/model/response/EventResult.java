package org.manas.appdirect.model.response;

import org.manas.appdirect.model.request.ResponseErrorCodes;

/**
 * @author Manas
 *
 */

public class EventResult {
	
	private String accountIdentifier;
	
	private String message;
	
	private boolean success;
	
	private ResponseErrorCodes errorCode;


	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public ResponseErrorCodes getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ResponseErrorCodes errorCode) {
		this.errorCode = errorCode;
	}

	@Override
	public String toString() {
		return "ResponseJson [accountIdentifier=" + accountIdentifier + ", message=" + message + ", success=" + success
				+ ", errorCode=" + errorCode + "]";
	}

	public String getAccountIdentifier() {
		return accountIdentifier;
	}

	public void setAccountIdentifier(String accountIdentifier) {
		this.accountIdentifier = accountIdentifier;
	}
	
	
}