package org.manas.appdirect.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.manas.appdirect.model.dao.SubscriptionDao;
import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.request.ResponseErrorCodes;
import org.manas.appdirect.model.request.UserEvent;
import org.manas.appdirect.model.response.EventResult;
import org.manas.appdirect.repository.UserRepository;
import org.manas.appdirect.service.base.DaoMapperService;
import org.manas.appdirect.service.base.HttpClientService;
import org.manas.appdirect.service.base.SubscriptionService;
import org.manas.appdirect.service.base.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

/**
 * 
 * @author Manas
 *
 */

@Service
public class UserServiceImpl implements UserService {

	private static final Logger logger = LoggerFactory
			.getLogger(UserServiceImpl.class);

	@Autowired
	private HttpClientService httpclient;

	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private DaoMapperService mapperService;

	@Override
	public UserDao getUser(String userId) {
		return userRepository.findOne(userId);
	}

	@Override
	public UserDao saveUser(UserDao userDao) {
		return userRepository.save(userDao);
	}

	@Override
	public SubscriptionDao assignUser(UserDao userDao, SubscriptionDao subscriptionDao) {

		Map<String, UserDao> subscribedUsers = subscriptionDao.getUsers();

		subscribedUsers.put(userDao.getUuid().toString(), userDao);

		subscriptionDao.setUsers(subscribedUsers);
		return subscriptionDao;
	}

	@Override
	public SubscriptionDao unAssignUser(UserDao userDao, SubscriptionDao subscriptionDao) {
		Map<String, UserDao> subscribedUsers = subscriptionDao.getUsers();
		String userUUID = userDao.getUuid().toString();
		subscribedUsers.remove(userUUID);
		subscriptionDao.setUsers(subscribedUsers);
		return subscriptionDao;

	}

	@Override
	public UserEvent retrieveUserEvent(String eventUrl) {
		UserEvent userEvent = null;
		try {
			HttpResponse response = httpclient.oAuthSignRequest(eventUrl);
			logger.debug("Response Code : "
					+ response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			logger.debug("Event details : " + result);
			Gson gson = new Gson();
			userEvent = gson.fromJson(result.toString(), UserEvent.class);
			logger.debug("eventurl data:" + userEvent.toString());
		} catch (Exception e) {
			logger.error("Error fetching event details", e);
		}
		return userEvent;
	}

	@Override
	public EventResult assignUser(UserEvent userEvent) {
		EventResult eventResult = null;
		SubscriptionDao subscriptionDao = subscriptionService.findByAccount(userEvent
				.getPayload().getAccount());

		if (subscriptionDao == null) {
			eventResult = subscriptionService.setFailureEventResult(
					"Subscribtion Account not found ",
					ResponseErrorCodes.ACCOUNT_NOT_FOUND);
		} else if (subscriptionDao.getUsers().containsKey(
				userEvent.getPayload().getUser().getUuid().toString())) {

			eventResult = subscriptionService.setFailureEventResult(
					"User Already exist for the subscription ",
					ResponseErrorCodes.USER_ALREADY_EXISTS);
		} else {
			eventResult = new EventResult();
			subscriptionDao = assignUser(mapperService.asDao(userEvent.getPayload().getUser()),
					subscriptionDao);
			subscriptionService.save(subscriptionDao);

			eventResult.setSuccess(true);
			eventResult
			.setMessage("Successfully assigned user");
			eventResult.setAccountIdentifier(subscriptionDao.getAccount()
					.getAccountIdentifier());
		}
		return eventResult;
	}

	@Override
	public EventResult unAssignUser(UserEvent userEvent) {
		EventResult eventResult = null;
		String userUUID = userEvent.getPayload().getUser().getUuid().toString();

		SubscriptionDao subscriptionDao = subscriptionService.findByAccount(userEvent
				.getPayload().getAccount());
		if (subscriptionDao == null) {
			eventResult = subscriptionService.setFailureEventResult(
					"Subscribtion Account not found ",
					ResponseErrorCodes.ACCOUNT_NOT_FOUND);
		} else if (!subscriptionDao.getUsers().containsKey(userUUID)) {

			eventResult = subscriptionService.setFailureEventResult(
					"User not found ", ResponseErrorCodes.USER_NOT_FOUND);
		} else {
			eventResult = new EventResult();
			subscriptionDao = unAssignUser(mapperService.asDao(userEvent.getPayload().getUser()),
					subscriptionDao);
			subscriptionService.save(subscriptionDao);

			eventResult.setSuccess(true);
			eventResult
			.setMessage("Successfully unassigned user");
			eventResult.setAccountIdentifier(subscriptionDao.getAccount()
					.getAccountIdentifier());
		}
		return eventResult;
	}

}
