package org.manas.appdirect.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.UUID;

import org.apache.http.HttpResponse;
import org.manas.appdirect.aop.EventRetrival;
import org.manas.appdirect.model.dao.AccountDao;
import org.manas.appdirect.model.dao.SubscriptionDao;
import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.enums.AccountStatus;
import org.manas.appdirect.model.enums.NoticeType;
import org.manas.appdirect.model.request.Account;
import org.manas.appdirect.model.request.Notice;
import org.manas.appdirect.model.request.Order;
import org.manas.appdirect.model.request.ResponseErrorCodes;
import org.manas.appdirect.model.request.SubscriptionEvent;
import org.manas.appdirect.model.request.User;
import org.manas.appdirect.model.response.EventResult;
import org.manas.appdirect.repository.SubscriptionRespository;
import org.manas.appdirect.service.base.DaoMapperService;
import org.manas.appdirect.service.base.HttpClientService;
import org.manas.appdirect.service.base.SubscriptionService;
import org.manas.appdirect.service.base.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

/**
 * @author Manas
 *
 */

@Service
public class SubscriptionServiceImpl implements SubscriptionService {

	@Autowired
	private HttpClientService httpclient;

	@Autowired
	private SubscriptionRespository subscriptionRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private DaoMapperService mapperService;

	private static final Logger logger = LoggerFactory
			.getLogger(SubscriptionServiceImpl.class);

	@Override
	public SubscriptionEvent retrieveSubscriptiontest(String sub) {
		return null;
	}

	@Override
	public SubscriptionEvent retrieveSubscriptionEvent(String eventUrl) {
		SubscriptionEvent subscription = null;
		try {
			HttpResponse response = httpclient.oAuthSignRequest(eventUrl);
			logger.debug("Response Code : "
					+ response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()));

			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			logger.debug("Event details : " + result);
			Gson gson = new Gson();
			subscription = gson.fromJson(result.toString(),
					SubscriptionEvent.class);
			logger.debug("eventurl data:" + subscription.toString());
		} catch (Exception e) {
			logger.error("Error fetching event details", e);
		}
		return subscription;
	}

	@Override
	public SubscriptionDao createSubscriptionFromEvent(
			SubscriptionEvent subscriptionEvent) {

		SubscriptionDao subscriptionDao = new SubscriptionDao();
		subscriptionDao.setMarketplace(mapperService.asDao(subscriptionEvent
				.getMarketplace()));
		subscriptionDao.setOrder(mapperService.asDao(subscriptionEvent
				.getPayload().getOrder()));
		subscriptionDao.setApplicationUuid(subscriptionEvent
				.getApplicationUuid());
		subscriptionDao.setCompany(mapperService.asDao(subscriptionEvent
				.getPayload().getCompany()));
		subscriptionDao.setCreator(mapperService.asDao(subscriptionEvent
				.getCreator()));
		// create account for the subscription
		subscriptionDao.setAccount(AccountDao.createNewAccount(UUID
				.randomUUID().toString()));
		return subscriptionDao;
	}

	@EventRetrival
	@Override
	public EventResult createSubscription(String eventUrl,
			SubscriptionEvent subscriptionEvent) {

		EventResult eventResult = new EventResult();

		if (subscriptionEvent == null) {
			eventResult = setFailureEventResult(
					"Unable to retrive Subscription event from appDirect",
					ResponseErrorCodes.UNKNOWN_ERROR);

		} else if (subscriptionEvent.getCreator() == null
				&& subscriptionEvent.getPayload().getUser() == null) {
			eventResult = setFailureEventResult(
					"User not found in subscription event",
					ResponseErrorCodes.USER_NOT_FOUND);
		} else {
			User user = subscriptionEvent.getCreator() == null ? subscriptionEvent
					.getPayload().getUser() : subscriptionEvent.getCreator();

			UserDao userDao = mapperService.asDao(user);

			logger.info("User is : " + user);
			logger.info("userDao is : " + userDao);

			SubscriptionDao subscriptionDao = createSubscriptionFromEvent(subscriptionEvent);
			userDao.setAccountStatus(AccountStatus.ACTIVE);
			// assign user in subscription
			subscriptionDao = userService.assignUser(userDao, subscriptionDao);
			subscriptionRepository.save(subscriptionDao);

			eventResult.setAccountIdentifier(subscriptionDao.getAccount()
					.getAccountIdentifier());
			eventResult.setMessage("Successfully created subscription!");
			eventResult.setSuccess(true);
			logger.debug("Successfully created subscription");
		}
		return eventResult;
	}

	@EventRetrival
	@Override
	public EventResult changeSubscription(String eventUrl,
			SubscriptionEvent subscriptionEvent) {
		EventResult eventResult = new EventResult();

		if (subscriptionEvent == null) {
			eventResult = setFailureEventResult(
					"Unable to retrive Subscription event from appDirect",
					ResponseErrorCodes.UNKNOWN_ERROR);
		} else if (subscriptionEvent.getPayload().getAccount() == null) {
			eventResult = setFailureEventResult(
					"Unable to find account from Subscription event Payload",
					ResponseErrorCodes.ACCOUNT_NOT_FOUND);
		} else {
			Account account = subscriptionEvent.getPayload().getAccount();
			String accountIdentifier = account.getAccountIdentifier();

			/*
			 * SubscriptionDao subscriptionfetched = subscriptionRepository
			 * .findByAccount(mapperService.asDao(account));
			 */
			SubscriptionDao subscriptionfetched = subscriptionRepository
					.findByAccountAccountIdentifier(accountIdentifier);
			logger.info(" subscriptionfetched : " + subscriptionfetched);
			// logger.info(" subscriptionfetchedtest : "+subscriptionfetchedtest);
			if (subscriptionfetched != null) {
				Order changedOrder = subscriptionEvent.getPayload().getOrder();
				subscriptionfetched.setOrder(mapperService.asDao(changedOrder));
				subscriptionRepository.save(subscriptionfetched);

				eventResult.setAccountIdentifier(accountIdentifier);
				eventResult.setMessage("Successfully changed subscription!");
				eventResult.setSuccess(true);

				logger.debug("Successfully changed subscription");

			} else {
				eventResult = setFailureEventResult(
						"Account not found in application database",
						ResponseErrorCodes.ACCOUNT_NOT_FOUND);
			}

		}
		return eventResult;
	}

	@EventRetrival
	@Override
	public EventResult cancelSubscription(String eventUrl,
			SubscriptionEvent subscriptionEvent) {
		EventResult eventResult = new EventResult();
		if (subscriptionEvent == null) {
			eventResult = setFailureEventResult(
					"Unable to retrive Subscription event from appDirect",
					ResponseErrorCodes.UNKNOWN_ERROR);
		} else if (subscriptionEvent.getPayload().getAccount() == null) {
			eventResult = setFailureEventResult(
					"Unable to find account from Subscription event Payload",
					ResponseErrorCodes.ACCOUNT_NOT_FOUND);
		} else {

			Account account = subscriptionEvent.getPayload().getAccount();
			String accountIdentifier = account.getAccountIdentifier();

			SubscriptionDao subscriptionfetched = subscriptionRepository
					.findByAccountAccountIdentifier(accountIdentifier);

			if (subscriptionfetched != null) {
				subscriptionfetched.getAccount().setStatus(
						AccountStatus.CANCELLED);
				subscriptionRepository.save(subscriptionfetched);

				eventResult.setAccountIdentifier(accountIdentifier);
				eventResult.setMessage("Successfully cancelled subscription!");
				eventResult.setSuccess(true);
			} else {
				eventResult = setFailureEventResult(
						"Account not found in application database",
						ResponseErrorCodes.ACCOUNT_NOT_FOUND);
			}

		}

		return eventResult;
	}

	@EventRetrival
	@Override
	public EventResult subscriptionNotice(String eventUrl,
			SubscriptionEvent subscriptionEvent) {
		EventResult eventResult = new EventResult();
		try {
			if (subscriptionEvent.getPayload().getAccount() == null
					|| subscriptionEvent.getPayload().getNotice() == null) {

				eventResult = setFailureEventResult(
						"Error while processing subscription status",
						ResponseErrorCodes.UNKNOWN_ERROR);
			} else {

				Account account = subscriptionEvent.getPayload().getAccount();
				String accountIdentifier = account.getAccountIdentifier();

				// retrieve subscription from database based on account
				SubscriptionDao subscriptionfetched = subscriptionRepository
						.findByAccountAccountIdentifier(accountIdentifier);

				if (subscriptionfetched == null) {
					eventResult = setFailureEventResult(
							"Account not found in application database",
							ResponseErrorCodes.ACCOUNT_NOT_FOUND);

				} else {

					Notice subscriptionNotice = subscriptionEvent.getPayload()
							.getNotice();
					if (NoticeType.CLOSED.equals(subscriptionNotice.getType()))
						subscriptionfetched.getAccount().setStatus(
								AccountStatus.CANCELLED);
					else if (NoticeType.DEACTIVATED.equals(subscriptionNotice
							.getType()))
						subscriptionfetched.getAccount().setStatus(
								AccountStatus.SUSPENDED);
					else if (NoticeType.REACTIVATED.equals(subscriptionNotice
							.getType()))
						subscriptionfetched.getAccount().setStatus(
								AccountStatus.ACTIVE);

					subscriptionRepository.save(subscriptionfetched);

					eventResult.setAccountIdentifier(accountIdentifier);
					eventResult
							.setMessage("Successfully processed subscription status notification!");
					eventResult.setSuccess(true);
				}
			}
		} catch (Exception e) {
			logger.error("Error while processing subscription status!", e);
			eventResult = setFailureEventResult(
					"Error while processing subscription status",
					ResponseErrorCodes.UNKNOWN_ERROR);
		}
		return eventResult;
	}

	@Override
	public EventResult setFailureEventResult(String msg,
			ResponseErrorCodes errorCode) {
		EventResult eventResult = new EventResult();
		eventResult.setSuccess(false);
		eventResult.setErrorCode(errorCode);
		eventResult.setMessage(msg);

		return eventResult;
	}

	@Override
	public SubscriptionDao findByAccount(Account account) {
		return subscriptionRepository
				.findByAccountAccountIdentifier(account.getAccountIdentifier());
	}

	@Override
	public SubscriptionDao save(SubscriptionDao subscriptionDao) {
		return subscriptionRepository.save(subscriptionDao);
	}

}
