package org.manas.appdirect.service.impl;

import java.io.IOException;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.manas.appdirect.service.base.HttpClientService;
import org.manas.appdirect.util.Constants;
import org.springframework.stereotype.Service;
/**
 * 
 * @author Manas
 *
 */
@Service
public class HttpClientServiceImpl implements HttpClientService{

	@Override
	public HttpResponse oAuthSignRequest(String url) throws ClientProtocolException, IOException, OAuthMessageSignerException, OAuthExpectationFailedException, OAuthCommunicationException {
		OAuthConsumer consumer = new CommonsHttpOAuthConsumer(Constants.OAUTH_CONSUMER_KEY,Constants.OAUTH_CONSUMER_SECRET);
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet httpGet = new HttpGet(url);
		httpGet.setHeader("Accept", "application/json");
		consumer.sign(httpGet);
		HttpResponse response = httpClient.execute(httpGet);
		return response;
	}

}
