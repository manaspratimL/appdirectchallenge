package org.manas.appdirect.service.impl;

import org.manas.appdirect.model.dao.AccountDao;
import org.manas.appdirect.model.dao.AddressDao;
import org.manas.appdirect.model.dao.CompanyDao;
import org.manas.appdirect.model.dao.MarketPlaceDao;
import org.manas.appdirect.model.dao.OrderDao;
import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.request.Account;
import org.manas.appdirect.model.request.Address;
import org.manas.appdirect.model.request.Company;
import org.manas.appdirect.model.request.MarketPlace;
import org.manas.appdirect.model.request.Order;
import org.manas.appdirect.model.request.User;
import org.manas.appdirect.service.base.DaoMapperService;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
/**
 * 
 * @author Manas
 *
 */
@Service
public class DaoMapperServiceImpl implements DaoMapperService {
	ModelMapper mapper= new ModelMapper();
	
	public UserDao asDao(User user) {
        return mapper.map(user, UserDao.class);
    }
	
	public AccountDao asDao(Account account) {
        return mapper.map(account, AccountDao.class);
    }
	
	public OrderDao asDao(Order order) {
        return mapper.map(order, OrderDao.class);
    }
	
	public CompanyDao asDao(Company company) {
        return mapper.map(company, CompanyDao.class);
    }
	
	public MarketPlaceDao asDao(MarketPlace marketPlace) {
        return mapper.map(marketPlace, MarketPlaceDao.class);
    }
	
	public AddressDao asDao(Address address) {
        return mapper.map(address, AddressDao.class);
    }
	
}
