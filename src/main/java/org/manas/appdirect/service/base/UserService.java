package org.manas.appdirect.service.base;

import org.manas.appdirect.model.dao.SubscriptionDao;
import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.request.UserEvent;
import org.manas.appdirect.model.response.EventResult;

/**
 * 
 * @author Manas
 *
 */
public interface UserService {
	
	public UserDao getUser(String userId);
	
	public UserDao saveUser(UserDao userDao);
	
	public EventResult assignUser(UserEvent userEvent);
	
	public SubscriptionDao assignUser(UserDao userDao, SubscriptionDao subscriptionDao);

	public EventResult unAssignUser(UserEvent userEvent);
    public SubscriptionDao unAssignUser(UserDao userDao, SubscriptionDao subscriptionDao);
    
    public UserEvent retrieveUserEvent(String eventUrl);
	
}
