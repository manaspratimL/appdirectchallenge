package org.manas.appdirect.service.base;

import org.manas.appdirect.model.dao.SubscriptionDao;
import org.manas.appdirect.model.request.Account;
import org.manas.appdirect.model.request.ResponseErrorCodes;
import org.manas.appdirect.model.request.SubscriptionEvent;
import org.manas.appdirect.model.response.EventResult;

/**
 * 
 * @author Manas
 *
 */
public interface SubscriptionService {

	public SubscriptionEvent retrieveSubscriptionEvent(String eventUrl);

	public SubscriptionEvent retrieveSubscriptiontest(String event);

	public SubscriptionDao createSubscriptionFromEvent(
			SubscriptionEvent subscription);

	public SubscriptionDao findByAccount(Account account);

	public EventResult createSubscription(String eventUrl,
			SubscriptionEvent subscriptionEvent);

	public EventResult changeSubscription(String eventUrl,
			SubscriptionEvent subscriptionEvent);

	public EventResult cancelSubscription(String eventUrl,
			SubscriptionEvent subscriptionEvent);

	public EventResult subscriptionNotice(String eventUrl,
			SubscriptionEvent subscriptionEvent);

	public SubscriptionDao save(SubscriptionDao subscriptionDao);

	public EventResult setFailureEventResult(String msg,
			ResponseErrorCodes errorCode);
}
