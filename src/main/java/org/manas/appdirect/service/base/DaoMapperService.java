package org.manas.appdirect.service.base;

import org.manas.appdirect.model.dao.AccountDao;
import org.manas.appdirect.model.dao.AddressDao;
import org.manas.appdirect.model.dao.CompanyDao;
import org.manas.appdirect.model.dao.MarketPlaceDao;
import org.manas.appdirect.model.dao.OrderDao;
import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.request.Account;
import org.manas.appdirect.model.request.Address;
import org.manas.appdirect.model.request.Company;
import org.manas.appdirect.model.request.MarketPlace;
import org.manas.appdirect.model.request.Order;
import org.manas.appdirect.model.request.User;
/**
 * 
 * @author Manas
 *
 */
public interface DaoMapperService {

	public UserDao asDao(User user);

	public AccountDao asDao(Account account);

	public OrderDao asDao(Order order);

	public CompanyDao asDao(Company company);

	public MarketPlaceDao asDao(MarketPlace marketPlace);

	public AddressDao asDao(Address address);

}
