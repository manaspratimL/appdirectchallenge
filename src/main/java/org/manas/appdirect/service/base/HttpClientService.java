package org.manas.appdirect.service.base;

import java.io.IOException;

import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
/**
 * 
 * @author Manas
 *
 */
public interface HttpClientService {

	public HttpResponse oAuthSignRequest(String url)
			throws ClientProtocolException, IOException,
			OAuthMessageSignerException, OAuthExpectationFailedException,
			OAuthCommunicationException;
}
