package org.manas.appdirect.aop;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
/**
 * 
 * @author Manas
 *
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface EventRetrival {}
