package org.manas.appdirect.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.manas.appdirect.model.request.ResponseErrorCodes;
import org.manas.appdirect.model.request.SubscriptionEvent;
import org.manas.appdirect.model.response.EventResult;
import org.manas.appdirect.service.base.SubscriptionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
/**
 * 
 * @author Manas
 *
 */
@Aspect
@Component
public class SubscriptionAdviceAspect {

	@Autowired
	private SubscriptionService subscriptionService;
	
	private static final Logger logger = LoggerFactory
			.getLogger(SubscriptionAdviceAspect.class);

	@Pointcut("within(@org.manas.appdirect.aop.EventRetrival *)")
	public void beanAnnotatedWithEventRetrival() {}

	@Pointcut("execution(public * *(..))")
	public void publicMethod() {}
	
	
	/*@Around("execution(@org.manas.appdirect.aop.EventRetrival * *(..)) && @annotation(EventRetrival)")*/
	@Around("execution(* *(..)) && @annotation(EventRetrival)")
	public Object beforeSubscriptionEvent(ProceedingJoinPoint joinPoint)
			throws Throwable {

		Object[] args = joinPoint.getArgs();
		String eventUrl = (String) args[0];
		logger.debug("******Event Url is *************** : "+eventUrl);
		// Get the Subscription event from appDirect
		SubscriptionEvent subscriptionEvent = subscriptionService
				.retrieveSubscriptionEvent(eventUrl);

		if (subscriptionEvent == null) {
			EventResult result = subscriptionService.setFailureEventResult(
					"Unable to Fetch event from appDirect Market Place",
					ResponseErrorCodes.UNKNOWN_ERROR);
			return result;
		} else {
			return (EventResult) joinPoint.proceed(new Object[] { args[0],
					subscriptionEvent });
		}

	}

}
