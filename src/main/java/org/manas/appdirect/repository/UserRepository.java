package org.manas.appdirect.repository;

import java.util.UUID;

import org.manas.appdirect.model.dao.UserDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * 
 * @author Manas
 *
 */
@Repository
public interface UserRepository extends JpaRepository<UserDao, String> {
	
	public UserDao findByEmail(String email);
	
	public UserDao findByEmailAndUuid(String email, UUID uuid);
}
