package org.manas.appdirect.repository;

import org.manas.appdirect.model.dao.AccountDao;
import org.manas.appdirect.model.dao.SubscriptionDao;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author Manas
 *
 */
@Repository
public interface SubscriptionRespository extends
		JpaRepository<SubscriptionDao, String> {

	@Query(value = "SELECT s FROM SUBSCRIPTION WHERE"
			+ " s.account.accountIdentifier =:accountIdentifier",
			nativeQuery = true)
	public SubscriptionDao findByAccountIdentifier(
			@Param("accountIdentifier") String accountIdentifier);
	
	public SubscriptionDao findByAccount(AccountDao account);
	public SubscriptionDao findByAccountAccountIdentifier(String accountIdentifier);
}
