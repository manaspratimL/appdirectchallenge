package org.manas.appdirect.service.impl;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.request.Address;
import org.manas.appdirect.model.request.User;
import org.manas.appdirect.service.base.DaoMapperService;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class DaoMapperServiceTest {

	@Before
	public void init(){
		
	}
	
	@Test
	public void testAsDao(){
		DaoMapperService mapperService=new DaoMapperServiceImpl();
		User user=new User();
		user.setAddress(new Address());
		user.getAddress().setCity("PUNE");
		UserDao userDao=mapperService.asDao(user);
		assertEquals(user.getAddress().getCity(),userDao.getAddress().getCity());
	}
}
