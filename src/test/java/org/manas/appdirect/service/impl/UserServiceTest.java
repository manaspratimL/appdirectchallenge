package org.manas.appdirect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.manas.appdirect.model.dao.AccountDao;
import org.manas.appdirect.model.dao.SubscriptionDao;
import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.request.Account;
import org.manas.appdirect.model.request.Payload;
import org.manas.appdirect.model.request.User;
import org.manas.appdirect.model.request.UserEvent;
import org.manas.appdirect.model.response.EventResult;
import org.manas.appdirect.service.base.DaoMapperService;
import org.manas.appdirect.service.base.SubscriptionService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

	@InjectMocks
	private UserServiceImpl userService;

	@Mock
	private DaoMapperService mapperService;

	@Mock
	private SubscriptionService subscriptionservice;

	private UserEvent userEvent;

	@Before
	public void init() {
		userEvent = mock(UserEvent.class);
		
		when(subscriptionservice.save(any(SubscriptionDao.class)))
		.thenReturn(mock(SubscriptionDao.class));
		
	}

	@Test
	public void testUserAssign() {
		SubscriptionDao mockedSubscription = new SubscriptionDao();
		mockedSubscription.setAccount(new AccountDao());
		mockedSubscription.getAccount().setAccountIdentifier(
				"assignUser");
		Map<String, UserDao> usermap=new HashMap<String, UserDao>();
		usermap.put("1",mock(UserDao.class));
		mockedSubscription.setUsers(usermap);

		when(subscriptionservice.findByAccount(any(Account.class))).thenReturn(
				mockedSubscription);
		when(userEvent.getPayload()).thenReturn(mock(Payload.class));
		when(userEvent.getPayload().getAccount()).thenReturn(
				mock(Account.class));
		User user=new User();
		user.setUuid("2");
		when(userEvent.getPayload().getUser()).thenReturn(user);
		
		UserDao userDao=new UserDao();
		userDao.setUuid("2");
		when(mapperService.asDao(any(User.class)))
		.thenReturn(userDao);
		
		EventResult result=userService.assignUser(userEvent);
		
		assertEquals(true, result.isSuccess());
		assertEquals(mockedSubscription.getAccount().getAccountIdentifier(),
				result.getAccountIdentifier());
		
	}

	@Test
	public void testUserUnassign() {
		SubscriptionDao mockedSubscription = new SubscriptionDao();
		mockedSubscription.setAccount(new AccountDao());
		mockedSubscription.getAccount().setAccountIdentifier(
				"unassignUser");
		Map<String, UserDao> usermap=new HashMap<String, UserDao>();
		usermap.put("1",mock(UserDao.class));
		mockedSubscription.setUsers(usermap);

		when(subscriptionservice.findByAccount(any(Account.class))).thenReturn(
				mockedSubscription);
		when(userEvent.getPayload()).thenReturn(mock(Payload.class));
		when(userEvent.getPayload().getAccount()).thenReturn(
				mock(Account.class));
		User user=new User();
		user.setUuid("1");
		when(userEvent.getPayload().getUser()).thenReturn(user);
		
		UserDao userDao=new UserDao();
		userDao.setUuid("1");
		when(mapperService.asDao(any(User.class)))
		.thenReturn(userDao);
		
		EventResult result=userService.unAssignUser(userEvent);
		
		assertEquals(true, result.isSuccess());
		assertEquals(mockedSubscription.getAccount().getAccountIdentifier(),
				result.getAccountIdentifier());
	}
}
