package org.manas.appdirect.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.manas.appdirect.model.dao.AccountDao;
import org.manas.appdirect.model.dao.OrderDao;
import org.manas.appdirect.model.dao.SubscriptionDao;
import org.manas.appdirect.model.dao.UserDao;
import org.manas.appdirect.model.enums.NoticeType;
import org.manas.appdirect.model.request.Account;
import org.manas.appdirect.model.request.Notice;
import org.manas.appdirect.model.request.Order;
import org.manas.appdirect.model.request.Payload;
import org.manas.appdirect.model.request.SubscriptionEvent;
import org.manas.appdirect.model.request.User;
import org.manas.appdirect.model.response.EventResult;
import org.manas.appdirect.repository.SubscriptionRespository;
import org.manas.appdirect.service.base.DaoMapperService;
import org.manas.appdirect.service.base.UserService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * 
 * @author Manas
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class SubscriptionServiceTest {

	@InjectMocks
	private SubscriptionServiceImpl subscriptionService;

	@Mock
	private SubscriptionRespository subscriptionRepository;

	@Mock
	private UserService userService;

	/*
	 * @Spy private DaoMapperService mapperService=new DaoMapperServiceImpl();
	 */

	@Mock
	private DaoMapperService mapperService;

	private SubscriptionEvent subscriptionEvent;

	private SubscriptionDao subscription;

	@Before
	public void init() {

		subscriptionEvent = mock(SubscriptionEvent.class);
		subscription = mock(SubscriptionDao.class);

		when(subscriptionRepository.save(any(SubscriptionDao.class)))
				.thenReturn(mock(SubscriptionDao.class));
		
		when(userService.assignUser(any(UserDao.class),
				any(SubscriptionDao.class))).thenReturn(subscription);

	}

	@Test
	public void testSubscriptionCreate() {
		when(subscriptionEvent.getPayload()).thenReturn(mock(Payload.class));
		when(subscriptionEvent.getPayload().getUser()).thenReturn(
				mock(User.class));
		when(subscriptionEvent.getCreator()).thenReturn(mock(User.class));
		when(subscription.getAccount()).thenReturn(mock(AccountDao.class));
		when(subscription.getAccount().getAccountIdentifier()).thenReturn(
				"123456");
		when(subscriptionRepository.findByAccountAccountIdentifier(anyString()))
				.thenReturn(mock(SubscriptionDao.class));
		when(mapperService.asDao(any(User.class))).thenReturn(
				mock(UserDao.class));

		EventResult result = subscriptionService.createSubscription(
				"sdhjashdjas", subscriptionEvent);
		assertEquals(true, result.isSuccess());
		assertEquals(subscription.getAccount().getAccountIdentifier(),
				result.getAccountIdentifier());
	}

	@Test
	public void testSubscriptionNotice() {
		when(subscriptionEvent.getPayload()).thenReturn(mock(Payload.class));
		when(subscriptionEvent.getPayload().getUser()).thenReturn(
				mock(User.class));
		when(subscriptionEvent.getCreator()).thenReturn(mock(User.class));
		when(subscription.getAccount()).thenReturn(mock(AccountDao.class));
		when(subscription.getAccount().getAccountIdentifier()).thenReturn(
				"subscriptionNotice");
		when(mapperService.asDao(any(User.class))).thenReturn(
				mock(UserDao.class));

		Notice subscriptionNotice = new Notice();
		subscriptionNotice.setType(NoticeType.DEACTIVATED);
		when(subscriptionEvent.getPayload().getNotice()).thenReturn(
				subscriptionNotice);

		SubscriptionDao mockedSubscription = new SubscriptionDao();
		mockedSubscription.setAccount(new AccountDao());
		mockedSubscription.getAccount().setAccountIdentifier(
				"subscriptionNotice");

		when(subscriptionRepository.findByAccountAccountIdentifier(anyString()))
				.thenReturn(mockedSubscription);

		Account mockedAccount = new Account();
		mockedAccount.setAccountIdentifier("subscriptionNotice");
		when(subscriptionEvent.getPayload().getAccount()).thenReturn(
				mockedAccount);

		EventResult result = subscriptionService.subscriptionNotice(
				"sdhjashdjas", subscriptionEvent);
		assertEquals(true, result.isSuccess());
		assertEquals(subscription.getAccount().getAccountIdentifier(),
				result.getAccountIdentifier());
	}

	@Test
	public void testSubscriptionChange() {
		when(subscriptionEvent.getPayload()).thenReturn(mock(Payload.class));
		when(subscriptionEvent.getPayload().getUser()).thenReturn(
				mock(User.class));
		when(subscriptionEvent.getCreator()).thenReturn(mock(User.class));
		when(subscription.getAccount()).thenReturn(mock(AccountDao.class));
		when(subscription.getAccount().getAccountIdentifier()).thenReturn(
				"subscriptionChanged");

		when(mapperService.asDao(any(Order.class))).thenReturn(
				mock(OrderDao.class));

		SubscriptionDao mockedSubscription = new SubscriptionDao();
		mockedSubscription.setAccount(new AccountDao());
		mockedSubscription.getAccount().setAccountIdentifier(
				"subscriptionChanged");

		when(subscriptionRepository.findByAccountAccountIdentifier(anyString()))
				.thenReturn(mockedSubscription);

		Account mockedAccount = new Account();
		mockedAccount.setAccountIdentifier("subscriptionChanged");
		when(subscriptionEvent.getPayload().getAccount()).thenReturn(
				mockedAccount);

		EventResult result = subscriptionService.changeSubscription(
				"sdhjashdjas", subscriptionEvent);
		assertEquals(true, result.isSuccess());
		assertEquals(subscription.getAccount().getAccountIdentifier(),
				result.getAccountIdentifier());
	}

	@Test
	public void testSubscriptionCancel() {

		SubscriptionDao mockedSubscription = new SubscriptionDao();
		mockedSubscription.setAccount(new AccountDao());
		mockedSubscription.getAccount().setAccountIdentifier(
				"subscriptionCancel");

		when(subscriptionRepository.findByAccountAccountIdentifier(anyString()))
				.thenReturn(mockedSubscription);

		when(subscriptionEvent.getPayload()).thenReturn(mock(Payload.class));
		when(subscriptionEvent.getPayload().getUser()).thenReturn(
				mock(User.class));
		Account mockedAccount = new Account();
		mockedAccount.setAccountIdentifier("subscriptionCancel");
		when(subscriptionEvent.getPayload().getAccount()).thenReturn(
				mockedAccount);
		// subscriptionEvent.getPayload().getAccount()
		when(subscriptionEvent.getCreator()).thenReturn(mock(User.class));
		when(subscription.getAccount()).thenReturn(mock(AccountDao.class));
		// when(subscription.getAccount().setStatus(anyObject());
		when(subscription.getAccount().getAccountIdentifier()).thenReturn(
				"subscriptionCancel");

		when(mapperService.asDao(any(User.class))).thenReturn(
				mock(UserDao.class));

		EventResult result = subscriptionService.cancelSubscription(
				"sdhjashdjas", subscriptionEvent);
		assertEquals(true, result.isSuccess());
		assertEquals(subscription.getAccount().getAccountIdentifier(),
				result.getAccountIdentifier());
	}
}
